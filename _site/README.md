new personal site based on [Hyde theme](https://github.com/poole/hyde)
archive page based on [Solar theme](https://github.com/mattvh/solar-theme-jekyll)
sidebar socials based on [minimal-mistakes](https://github.com/mmistakes/minimal-mistakes/)
