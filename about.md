---
layout: page
title: About
pagetitle: About me
---

### Bi Cheng Who?

I'm a 2<sup>nd</sup> year statistics PhD student at UW-Madison. Before that, I graduated from the University of Rochester, with a dual B.S. in math & physics. And before that, I grew up in Vancouver, Canada with my pet moose in a hand-built igloo where I drank maple syrup and played hockey.

### Is that a joke?

maybe

### What are you studying?

Right now I'm taking time series, decision trees, and statistical computing. This semester I have fewer exams but more projects, which I actually prefer. I'm also researching microbiome statistics with Dr. Zhengzheng Tang (see CV for details).

### What do you do 4 fun?

Watch movies, listen to music, read fantasy, drink with friends, and skate when the weather is nice.


