---
layout: page
title: CV
pagetitle: CV
---

<!-- <embed src="https://drive.google.com/viewerng/viewer?embedded=true&url={{ site.baseurl }}/assets/documents/cv.pdf" class="viewer-google"> 
<embed src="{{ site.baseurl }}/assets/documents/cv.pdf" type="application/pdf" class="viewer-native">
-->

<embed src="https://drive.google.com/viewerng/viewer?embedded=true&url={{ site.baseurl }}/assets/documents/cv.pdf" class="viewer" style="margin-top:20px;width:100%;margin-left:auto;margin-right:auto;height:500px;">

<a href="{{ site.baseurl }}/assets/documents/cv.pdf" style="text-decoration:underline;" download>download CV</a>

