---
layout: post
title: The Mike Cammilleri Organ Trio
---

Went out tonight with a friend to see the [Mike Cammilleri Organ Trio](http://www.hammondorgannite.com/) perform live at [Cafe Coda](https://cafecoda.club/).
I've been meaning to see Mike play for a while now, and what a performance it was!
I didn't capture his best solo, but here's a nice clip from near the end of the first set.

<video class="post-video" controls>
  <source src="{{ site.baseurl }}/assets/videos/2018-11-16-organ-trio.mp4" type="video/mp4">
</video>

Sadly we had to leave early due to other plans, but I'll definitely be back for more!

<i>#andreaisrunningerrands</i>

